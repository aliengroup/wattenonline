from pyglet.resource import image as reimage
from pyglet import image as pyimage
from pyglet.image.codecs.png import PNGImageDecoder
from pyglet.sprite import Sprite
"""
	addable Tuples  (1,2)+(4,3)= (5,5)
"""
class WVec2d(tuple):
	def __add__(self, other:tuple):

		if len(self) != len(other):
			return NotImplemented

		else:
			return WVec2d(x+y for x,y in zip(self, other))

	def __sub__(self, other:tuple):

		if len(self) != len(other):
			 return NotImplemented

		else:
			 return WVec2d(x-y for x,y in zip(self,other))



"""
	WCard container with small and big image
"""
class WCard:
	def __init__(self, url_small:str, url_big:str, x:int = 0, y:int = 0, name:int = 0):
		self.image_small = reimage(url_small)
		self.image_big = reimage(url_big)

		self.size_small = WVec2d((self.image_small.width, self.image_small.height))
		self.size_big = WVec2d((self.image_big.width, self.image_big.height))

		# to align the big centre with the small centre
		x_offset = self.image_big.width/2 - self.image_small.width/2

		self.pos_small = WVec2d((x,y))
		self.pos_big = WVec2d((x-x_offset,y))

		# big is not visible
		self.small_is_visable = True

	def Wdraw(self):

		if self.small_is_visable:
			self.image_small.blit(self.pos_small[0], self.pos_small[1])

		else:
			self.image_big.blit(self.pos_big[0], self.pos_big[1])

	def Wis_over(self, x:int, y:int):

		# if the coords are inside the small image
		pos2 : tuple = self.pos_small + self.size_small
		return x<pos2[0] and self.pos_small[0]<x and y<pos2[1] and self.pos_small[1]<y


	def Wupdate_position(self, x:int, y:int):

		pass

	def __repr__(self):
		return f"CARD AT: {self.pos_small}, IS VISABLE: {self.small_is_visable}"

"""
	for starting/connecting buttons and menu
"""
class WMenuButton:
	def __init__(self, url:str, x:int = 0, y:int = 0):
		self.start_sprite = Sprite(pyimage.load(url, decoder=PNGImageDecoder()),x=x,y=y)
		self.pos = WVec2d((x,y))
		self.size = WVec2d((self.start_sprite.width, self.start_sprite.height))

		self.is_visable = True
		self.is_pressed = False

	def Wdraw(self):
		if self.is_visable:
			self.start_sprite.draw()


	def Wis_over(self, x:int, y:int):

		# if the coords are inside the small image
		if self.is_visable:
			pos2 : tuple = self.pos + self.size
			return x<pos2[0] and self.pos[0]<x and y<pos2[1] and self.pos[1]<y
		return False




