import pyglet
from pyglet.window import mouse
import socket

from lib import Wlib
HOST= '127.0.0.1' 
PORT = 61111      


"""
	 handles the socket connection and handles the game gfx
"""
class WGameHandler:

	# ajust url_small
	def __init__(self, name:str, screen_size:tuple):
		self.all_cards = [Wlib.WCard(url_big=f"gfx/{i}.jpeg", url_small=f"gfx/1s.jpg") for i in range(1, 33)]
		
		self.name = name
		
		self.is_connected = False
		
		self.screen_size = screen_size
		
		space = self.screen_size[0]/5
		pos = int((self.screen_size[0]-2*space)/5)
		self.my_cards = [Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/8.jpeg', x=space, y=20),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/16.jpeg', x=space+pos, y=40),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/24.jpeg', x=space+2*pos, y=60),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/32.jpeg', x=space+3*pos, y=40),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/7.jpeg', x=space+4*pos, y=20)]

		self.played_cards = [Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/8.jpeg',
										x=self.screen_size[0]/4, y=self.screen_size[1]/2),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/16.jpeg', 
										x=3*self.screen_size[0]/4-150, y=self.screen_size[1]/2)]

	"""
		trying to connect to the server; return False if it failed
	"""	
	def Wconnect(self):
		try:

			socket_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			socket_connection.connect((HOST, PORT))
			self.is_connected = True
			return True

		except Exception as e:
			print("Couldn't Connect;",e)
			return False

	def Wreceive(self):
		pass

	def Wsend_message(self):
		pass

"""
	subclass for the buttons and username entry
"""
class WMenu:
	def __init__(self, screen_size : tuple):

		self.screen_size = screen_size
		
		self.play_button = Wlib.WMenuButton(url='gfx/button_play.png', 
											x=self.screen_size[0]/2-65, 
											y=self.screen_size[1]/2)
		
		self.del_button = Wlib.WMenuButton(url='gfx/button_delete-username.png', 
											x=self.screen_size[0]/2-132, 
											y=self.screen_size[1]/2-170)

		self.name_string = 'Username'
		self.name_label_is_visable = True
		self.name_label = pyglet.text.Label(self.name_string, 
											font_name='OpenSans',
											font_size=50,
											x=self.screen_size[0]/2-105,
											y=self.screen_size[1]/2-70)

		self.connection_error_label_is_visable = False
		self.connection_error_label = pyglet.text.Label('Try Again', 
											font_name='OpenSans',
											font_size=40,
											x=self.screen_size[0]/2-70,
											y=self.screen_size[1]/2+200)

	def Wdraw(self):
		if self.play_button.is_visable:
			self.play_button.Wdraw()

		if self.del_button.is_visable:
			self.del_button.Wdraw()
			
		if self.name_label_is_visable:
			self.name_label.draw()	

		if self.connection_error_label_is_visable:
			self.connection_error_label.draw()		

	def Wtext_handler(self, text):
		if not self.play_button.is_pressed:
			if self.name_string == 'Username':
				self.name_string=''
			if len(self.name_string) != 9:
				self.name_string = self.name_string + text
				self.name_label.text = self.name_string

	def Wpress_handle(self, x,y):
		"""
			returns True if play has been pressed
		"""
		if self.play_button.Wis_over(x,y):
			self.play_button.is_visable = False
			self.del_button.is_visable = False
			self.name_label_is_visable = False
			self.play_button.is_pressed = True
			self.connection_error_label_is_visable = False

			return True

		if self.del_button.Wis_over(x,y):
			self.name_string = 'Username'
			self.name_label.text = self.name_string

			return False

	def Wreset_buttons(self):
		"""
			resets buttons to before being pressed
		"""

		self.play_button.is_visable = True
		self.del_button.is_visable = True
		self.name_label_is_visable = True
		self.play_button.is_pressed = False

		# sets connection error label
		self.connection_error_label_is_visable = True

"""	
	main graphi6s
"""
class WMainWindow(pyglet.window.Window):
	def __init__(self):
		
		super(WMainWindow, self).__init__(fullscreen=True)

		display = pyglet.canvas.Display()
		screen = display.get_default_screen()

		self.screen_size = Wlib.WVec2d((screen.width, screen.height))
		print(self.screen_size)

		"""
			game widgets holds all current cards and play stuff
		"""
		self.game_widgets = []
		self.Winit()

	def Winit(self):
		"""
				generate game gfx obj here
		"""
		self.back_ground = pyglet.sprite.Sprite(pyglet.image.load('gfx/Barbg.jpg'))
		self.menu = WMenu(self.screen_size)
		self.game = WGameHandler(name=self.menu.name_string, screen_size=self.screen_size)

		
	def on_draw(self):
		"""
			on_draw gets called every draw event
		"""
		self.clear()
		self.back_ground.draw()

		if not self.menu.play_button.is_visable:
			for obj in self.game_widgets:
				obj.Wdraw()

		self.menu.Wdraw()

	def on_text(self, text):
		"""
			gets called on key input but only text symbols
		"""

		self.menu.Wtext_handler(text)
			
	def on_key_press(self, symbol, modifier):
	    if symbol == pyglet.window.key.ESCAPE:
	    	self.close()

	    if not self.menu.play_button.is_pressed and symbol == pyglet.window.key.BACKSPACE:
	    	self.menu.name_string = self.menu.name_string[:-1]
	    	self.menu.name_label.text = self.menu.name_string

	def on_mouse_press(self, x, y, button, modifiers):
		if self.menu.Wpress_handle(x,y):
			if not self.game.is_connected:
				if self.game.Wconnect():
					self.game_widgets += self.game.my_cards
					self.game_widgets += self.game.played_cards

				else: 
					# failed to connect
					self.menu.Wreset_buttons()

			
	def on_mouse_motion(self,x , y, button, modifiers):

		if self.menu.play_button.is_pressed:
			for card in self.game.my_cards:
				if card.Wis_over(x,y):
					card.small_is_visable = False
				else:
					card.small_is_visable = True


if __name__ == '__main__':

	window = WMainWindow()

	#event_logger = pyglet.window.event.WindowEventLogger()
	#window.push_handlers(event_logger)
	pyglet.app.run()
