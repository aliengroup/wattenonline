import socket
import threading

HOST = '127.0.0.1' 
PORT = 61111
ROOMS = dict()


"""
	Gameroom connects users and shares the game info between them
"""
class WSGameRoom:

	def __init__(self, id:str):

		self.id = id
		self.users = list()

	def WSadd_user(self, user):
		"""
		trys to add new user to Gameroom; returns True if it worked
		"""
		if len(self.users) == 2:
			return False

		else:
			self.users.append(user)
			print(f'Room {self.id}: {user.name} joined')
			return True

	def WSwatten_start(self):
		pass

"""
	class handling user send and rec
"""
class WSUser:

	def __init__(self, connection, address):
		self.connection = connection
		self.address = address
		self.name = self.WSreceive_message()

	def WSsend_message(self, text:str):

		pass

	def WSreceive_message(self):
		# wait for single message; already pickled
		try: 
			data = pickle.loads(self.mConnection.recv(1024))
			return data

		except:
			# returning None type to raise an exception
			return None




if __name__=='__main__':

	socket_connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket_connection.bind((HOST, PORT))
	socket_connection.listen(5)

	while True:
		connection, address = socket_connection.accept()
		new_user = WSUser(connection, address)

		print("new_user connected")
		# with matchmaking

		game_id = 500

		while True:

			if game_id not in ROOMS:
				ROOMS[game_id] = WSGameRoom(id=game_id)


			if ROOMS[game_id].WSadd_user(new_user):
				if len(ROOMS[game_id].users) == 2:
					# start game
					game_thread = threading.Thread(target=ROOMS[game_id].WSwatten_start)
					game_thread.start()
				break


			else:
				# room full
				game_id += 11


