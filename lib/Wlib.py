from pyglet.resource import image as pyimage
"""
	addable Tuples  (1,2)+(4,3)= (5,5)
"""
class WVec2d(tuple):
	def __add__(self, other):

		if len(self) != len(other):
			return NotImplemented

		else:
			return WVec2d(x+y for x,y in zip(self, other))

	def __sub__(self, other):

		if len(self) != len(other):
			 return NotImplemented

		else:
			 return WVec2d(x-y for x,y in zip(self,other))



"""
	WCard container with small and big image
"""
class WCard:
	def __init__(self, url_small:str, url_big:str, x:int =0, y:int =0):
		self.image_small = pyimage(url_small)
		self.image_big = pyimage(url_big)

		self.size_small = WVec2d((self.image_small.width, self.image_small.height))
		self.size_big = WVec2d((self.image_big.width, self.image_big.height))

		# to align the big centre with the small centre
		x_offset = self.image_big.width/2 - self.image_small.width/2

		self.pos_small = WVec2d((x,y))
		self.pos_big = WVec2d((x-x_offset,y))

		# big is not visible
		self.small_is_visable = True

	def Wdraw(self):

		if self.small_is_visable:
			self.image_small.blit(self.pos_small[0], self.pos_small[1])

		else:
			self.image_big.blit(self.pos_big[0], self.pos_big[1])

	def Wis_over(self, x, y):

		# if the coords are inside the small image
		pos2 : tuple = self.pos_small + self.size_small
		return x<pos2[0] and self.pos_small[0]<x and y<pos2[1] and self.pos_small[1]<y


	def Wupdate_position(self, x, y):

		pass

	def __repr__(self):
		return f"Card at: {self.pos1},{self.pos2}"
