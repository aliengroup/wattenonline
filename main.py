import pyglet
from pyglet.window import mouse

from lib import Wlib

"""

"""
class WMainWindow(pyglet.window.Window):
	def __init__(self):
		super().__init__(width=1920, height=1080, fullscreen=True)

		self.back_ground = pyglet.sprite.Sprite(pyglet.image.load('gfx/Barbg.jpg'))

		self.Winit()

	def Winit(self):

		self.my_cards = [Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/8.jpeg', x=440, y=20),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/16.jpeg', x=648, y=40),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/24.jpeg', x=856, y=60),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/32.jpeg', x=1064, y=40),
					Wlib.WCard(url_small='gfx/1s.jpg', url_big='gfx/7.jpeg', x=1272, y=20),]

	def on_draw(self):
		self.clear()
		self.back_ground.draw()
		
		for card in self.my_cards:
			card.Wdraw()


	def on_mouse_press(self, x, y, button, modifiers):
		pass
		
	def on_mouse_motion(self,x , y, button, modifiers):

		for card in self.my_cards:
			if card.Wis_over(x,y):
				card.small_is_visable = False
			else:
				card.small_is_visable = True


window = WMainWindow()

#event_logger = pyglet.window.event.WindowEventLogger()
#window.push_handlers(event_logger)
pyglet.app.run()

